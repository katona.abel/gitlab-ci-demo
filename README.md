Running the tests locally & the first pipeline

Useful commands:
* Check docker and docker-compose
  * docker --help (docker usage help)
  * docker-compose --help (docker-compose usage help)
* Starting containers using docker-compose
  * docker-compose up (start the containers, attached (shows output))
  * docker-compose up -d (start the containers, detached (does not show output))
* Shutting down containers
  * docker-compose down
  * docker stop $(docker ps -a -q) (stops ALL containers)
* Listing running container
  * docker ps